import os
from string import letters

# to enable special routing creation capability
from webapp2 import WSGIApplication, Route

# setup the template environment
# Set useful fields
root_dir = os.path.dirname(__file__)
template_dir = os.path.join(root_dir, 'templates')

PAGE_RE = r'(/(?:[a-zA-Z0-9_-]+/?)*)'

# routing and handlers for each url
routes = [
    Route(r'/', handler='handlers.sample.Front', name='landing_page'),
    Route(r'/bbc', handler='handlers.bbc.Front', name='bbc'),
    Route(r'/learning-js', handler='handlers.learning_js.Front', name='learning-js'),
    Route(r'/learning-js/disappearing-circle', handler='handlers.learning_js.DisappearingCircle',
     name='dis_circle'),
    Route(r'/learning-js/reaction', handler='handlers.learning_js.Reaction',
     name='js_reaction'),
    Route(r'/learning-js/jquery', handler='handlers.learning_js.JQuery',
     name='js_jquery'),
    Route(r'/learning-js/ajax', handler='handlers.learning_js.Ajax',
     name='ajax'),
    Route(r'/learning-js/jquery/ui', handler='handlers.learning_js.JQueryUI',
     name='jquery_ui'),
    Route(r'/codeplayer', handler='handlers.codeplayer.Front',
     name='codeplayer_front'),
    Route(r'/bootstrap', handler='handlers.bootstrap.Front',
     name='bootstrap'),
    Route(r'/outfit', handler='handlers.outfit.Outfit',
     name='outfit'),
    Route(r'/hearthstone', handler='handlers.hearthstone.DeckBuilder',
     name='hearthstone'),
    Route(r'/goals', handler='handlers.goals.Front',
     name='goals')
    ]

# main app
app = WSGIApplication(routes=routes, debug=True)
