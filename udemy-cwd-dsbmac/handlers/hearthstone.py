"""
Handler for outfit app
"""

from handlers.base import AppHandler
from datetime import date
from time import gmtime, strftime
import logging

class DeckBuilder(AppHandler):
  """
    deckbuilding app
  """

  def get(self):
    self.render("hearthstone.html")

