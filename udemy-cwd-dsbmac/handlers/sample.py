"""
Handler for sample site
"""

from handlers.base import AppHandler
from datetime import date
from time import gmtime, strftime
import logging

class Front(AppHandler):
  """
  bootstrap section
  """

  def get(self):
    self.render("landing_page.html")

