"""
Handler for outfit app
"""

from handlers.base import AppHandler
from datetime import date
from time import gmtime, strftime
import logging

class Outfit(AppHandler):
  """
  outfit page
  """

  def get(self):
    self.render("outfit.html")

