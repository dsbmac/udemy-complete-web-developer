"""
  Handler for Learing Javascript Chapter
"""

from handlers.base import AppHandler
from datetime import date
from time import gmtime, strftime
import logging

class Front(AppHandler):
    """
        html css previewing program
    """
    def get(self):
        self.render("codeplayer.html")

