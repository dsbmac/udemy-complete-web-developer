"""
Handler for goals website
"""

from handlers.base import AppHandler
from datetime import date
from time import gmtime, strftime
import logging

class Front(AppHandler):
  """
  front page section
  """

  def get(self):
    self.render("goals.html")

