"""
  Handler for BBC clone
"""

from handlers.base import AppHandler
from datetime import date
from time import gmtime, strftime
import logging


class Front(AppHandler):

    def get(self):
        d = date.today()
        header_date = d.strftime("%d %B %Y")
        last_updated = strftime("%H:%M", gmtime())
        self.render("bbc.html", header_date=header_date, last_updated=last_updated)