"""
  Handler for Learing Javascript Chapter
"""

from handlers.base import AppHandler
from datetime import date
from time import gmtime, strftime
import logging

class Front(AppHandler):
    """
        simple js exercises
    """
    def get(self):
        self.render("learning_js.html")


class DisappearingCircle(AppHandler):
    """
        DisappearingCircle MiniChallenge
        Click on a red circle and make it disappear
    """

    def get(self):
        self.render("disappearing_circle.html")


class Reaction(AppHandler):
    """
        Reaction mini game. Click on the square and displays
        the reaction time.
    """

    def get(self):
        self.render("reaction.html")


class JQuery(AppHandler):

    def get(self):
        self.render("learning_jquery.html")

class JQueryUI(AppHandler):

    def get(self):
        self.render("jquery_ui.html")

class Ajax(AppHandler):

    def get(self):
        self.render("ajax.html")